# -*- coding: utf-8 -*-
{
    'name': "product_template_pdf_report",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Kanaung Myanmar Software Solutions",
    'website': "http://www.yourcompany.com",
    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base', 'product'],
    'data': [
        'views/views.xml',
    ],
    'demo': [
    ],
}