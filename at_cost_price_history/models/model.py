from odoo import fields, models, api, tools, _
	
class ProductPH(models.Model):
	_inherit = 'product.price.history'

	@api.one
	def change_cost(self):
		print(" => called change_cost fun")
		for record in self:
			cost_price = record.cost
			get_product = self.env['product.product'].search([('id', '=', record.product_id.id)])
			# change product cost
			get_product.standard_price = record.cost
		return True

class ProductPricelistItem(models.Model):
	_inherit = 'product.pricelist.item'

	base = fields.Selection(
			default='standard_price'
		)

