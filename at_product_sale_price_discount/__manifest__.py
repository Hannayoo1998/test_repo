{
    'name': "at_product_sale_price_discount",
    'description': """
        To Track the sale discount price of a product in pricelist.
    """,
    'author': "Kanaung Myanmar Software Solution",
    'version': '0.1',
    'depends': [
                'base', 
                'product'
                ],
    'data': [
        'views/views.xml',
    ],
}