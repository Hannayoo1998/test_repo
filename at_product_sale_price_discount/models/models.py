from odoo import fields, models, api, tools, _

class ProductPro(models.Model):
	_inherit = 'product.product'
	
	# sale_discount = fields.Float(null = True)

class ProductPList(models.Model):
	_inherit = "product.pricelist"

	@api.one
	def calculate(self, obj_id):
		pricelist = self.env['product.pricelist'].search([('id', '=', obj_id)])
		# search product in pricelist to calculate discount
		if pricelist:
			# get sub pricelist item
			get_pricelist_items = self.env['product.pricelist.item'].search([('pricelist_id', '=', pricelist.id)], order = "id")

			for ii in get_pricelist_items:
				if ii.applied_on == "3_global":
					get_global_products  = self.env['product.product'].search([])
					get_sale_discount = self.calculate_final_price(ii, get_global_products)
					
				if ii.categ_id.id:
					get_categ_id_products  = self.env['product.product'].search([('product_tmpl_id.categ_id', '=', ii.categ_id.id)])
					get_sale_discount = self.calculate_final_price(ii, get_categ_id_products)
					
				if ii.product_tmpl_id.id:
					get_t_product  = self.env['product.product'].search([('product_tmpl_id', '=', ii.product_tmpl_id.id)])
					get_sale_discount = self.calculate_final_price(ii, get_t_product)
					
				if ii.product_id.id:
					get_product  = self.env['product.product'].search([('id', '=', ii.product_id.id)])
					get_sale_discount = self.calculate_final_price(ii, get_product)
				
		return True
	
	@api.multi 
	def calculate_final_price(self, price_litem, product):
		PriceHistory = self.env["product.price.history"]
		get_pricelist_price = 0
		fp_percen = price_litem.percent_price
		fp_discount = price_litem.price_discount
		fp_surcharge = price_litem.price_surcharge
		fp_round = price_litem.price_round
		fp_percen = price_litem.percent_price
		for i in product:
			PT = self.env['product.template']
			par_t_product = PT.search([('id', '=', i.id)])
			if price_litem.compute_price == "fixed":
				get_pricelist_price = price_litem.fixed_price

			if price_litem.compute_price == "percentage":
				get_pricelist_price = i.standard_price * (1 - fp_percen / 100)  # ****

			if price_litem.compute_price == "formula":
				if price_litem.base == 'standard_price':
					if fp_discount :
						get_pricelist_price = i.standard_price * (1 - fp_discount / 100)
					if fp_surcharge :
						get_pricelist_price += fp_surcharge
					if fp_round :
						get_pricelist_price = tools.float_round(get_pricelist_price, precision_rounding=fp_round) 
				else:
					get_pricelist_price = i.list_price
			par_t_product.write({
					'sale_discount': round(get_pricelist_price),
					'list_price': 0
				})
		return True

	@api.multi
	def write(self, vals):
		# write changes 
		rec = super(ProductPList, self).write(vals)
		self.calculate(self.id)
		return rec
		
	@api.model
	def create(self, vals):
		rec = super(ProductPList, self).create(vals)
		self.calculate(self.id)
		return rec

class ProductTem(models.Model):
	_inherit = 'product.template'

	standard_price  = fields.Float(store = True)
	type = fields.Selection(default = "product")

	