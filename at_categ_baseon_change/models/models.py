# -*- coding: utf-8 -*-

from odoo import models, fields, api

class at_categ_baseon_change(models.Model):
    _inherit = 'product.pricelist'

    # name = fields.Char()
    # value = fields.Integer()
    # value2 = fields.Float(compute="_value_pc", store=True)
    # description = fields.Text()

    # @api.depends('value')
    # def _value_pc(self):
    #     self.value2 = float(self.value) / 100

    @api.multi
    def call_me(self):
    	recs = self.env['product.pricelist.item'].search(['&', ('compute_price', '=', 'formula'), ('base', '!=', 'standard_price')])
    	print("recs --> ", recs)
    	recs.write({
    		'base': 'standard_price'
    		})
    	return True