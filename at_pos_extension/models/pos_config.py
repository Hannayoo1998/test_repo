
from odoo import api, fields, models

class PosConfig(models.Model):
	_inherit = 'pos.config'

	@api.depends('session_ids')
	def _compute_current_session(self):
		for pos_config in self:
			session = pos_config.session_ids.filtered(lambda r: r.user_id.id == self.env.uid and \
				not r.state == 'closed' and \
				not r.rescue)
			# sessions ordered by id desc
			pos_config.current_session_id = session and session[0].id or False
			pos_config.current_session_state = session and session[0].state or False
			if pos_config.current_session_state is False and self.env.uid == 1:
					session = pos_config.session_ids.filtered(lambda r: not r.state == 'closed' and not r.rescue)
					pos_config.current_session_id = session and session[0].id
					pos_config.current_session_state = session and session[0].state 
	
	@api.multi
	def open_existing_session_cb(self):
		# close session button
		for pos_config in self:
			session = pos_config.session_ids.filtered(lambda r :not r.state == 'closed' and \
				not r.rescue)
			pos_config.current_session_id = session and session[0].id or False

			self.ensure_one()
			return self._open_session(pos_config.current_session_id.id)
	
	@api.multi
	def open_session_cb(self):
		if self.current_session_id.state == 'opened':
			return self.open_ui()
		else:
			self.current_session_id = self.env['pos.session'].create({
				'user_id': self.env.uid,
				'config_id': self.id
			})
		
		return self._open_session(self.current_session_id.id)
