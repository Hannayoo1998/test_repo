from odoo import fields, models, api

class ConnectCategories(models.Model):
	_inherit = 'product.category'

	@api.model
	def create(self, vals):
		POS = self.env['pos.category']
		PC = self.env['product.category']
		# create in product category
		create_record_in_pc = super(ConnectCategories, self).create(vals)
		
		# create in pos category
		if not vals['parent_id']:
			create_record_set = POS.create({
				'name': '%s'% (vals['name'])
			})
		else:
			parent_id_inPC = PC.search([('id', '=', vals['parent_id'])])
			get_parent_inPC = PC.browse([parent_id_inPC])
			
			parent_id_inPOS = POS.search([('name', '=', get_parent_inPC.id.complete_name)])
			if not parent_id_inPOS.id:
				# create parent first
				create_record_set = POS.create({
					'name': '%s'% (get_parent_inPC.id.complete_name)
				})
				parent_id_inPOS_for_child = POS.search([('name', '=', get_parent_inPC.id.complete_name)])
				
				# create category
				create_record_set = POS.create({
					'name': '%s'% (vals['name']),
					'parent_id': parent_id_inPOS_for_child.id
				})
			else:
				create_record_set = POS.create({
					'name': '%s'% (vals['name']),
					'parent_id': parent_id_inPOS.id
				})

		return create_record_in_pc

	#@api.multi
	#def write(self, vals):
		#POS = self.env['pos.category']
		#PC = self.env['product.category']
		#get_i = PC.search([('id', '=', self.id)])
		#get_i_obj = PC.browse([get_i.id])
		
		# change name
		#if 'name' in vals:
		#	get_cats_in_pos_id = POS.search([('name', '=', get_i_obj.name)]).id
		#	get_cats_in_pos = POS.browse([get_cats_in_pos_id])
		#	change_name = get_cats_in_pos.id.write({
		#		'name': vals['name']
		#	})

		# change parent
		#elif 'parent_id' in vals:
		#	print('Parent')
			# get_text_in_pos = POS.search([('id', '=', vals['parent_id'])])
			# print(get_text_in_pos.name)
			# get_cats_in_pos = POS.search([('name', 'like', get_text_in_pos.name)])
		
		# update in product category
		#create_record_in_pc = super(ConnectCategories, self).write(vals)
		#return create_record_in_pc
