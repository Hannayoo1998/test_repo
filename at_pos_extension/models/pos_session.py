from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import UserError, ValidationError

class PosSession(models.Model):
	_inherit = "pos.session"

	user_id = fields.Many2one(
        'res.users', string='Cashier',
        required=True,
        index=True,
        readonly=True,
        states={'opening_control': [('readonly', False)]},
        default= lambda self: self.env.uid
        )
