{
    'name': 'Anytime POS Extension',
    'description':'',
    'summary':'POS product extension (Anytime)',
    'author': 'Kanaung Myanmar Software Solutions',
    'website': '',
    'depends':['purchase', 'hr_expense', 'point_of_sale', 'product', 'base', 'account'],
    'category': 'POS',
    'application':True,
    'qweb':[
        'static/src/xml/pos.xml'
        ],
    'data':[
        'views/session_close.xml',
        'views/point_of_sale.xml',
        'views/pos_money_view.xml',
        'security/ir.model.access.csv',
        'security/todo_access_rules.xml',
        ],
}