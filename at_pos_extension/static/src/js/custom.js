odoo.define('pos_ext.models', function (require) {
    "use strict";

    var models = require('point_of_sale.models');
    
    var custom_date = models.Order.extend({
    	initialize_validation_date: function () {
	        this.validation_date = new Date();
	        this.formatted_validation_date = this.validation_date.getHours() + "Hours";
	    },
    })
})