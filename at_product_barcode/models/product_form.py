# -*- coding: utf-8 -*-

import math
import re
import random

from odoo import api, models, _
from odoo.exceptions import UserError

class ProductAutoBarcode(models.Model):
    _inherit = "product.template"

    @api.model
    def create(self, vals):
        res = super(ProductAutoBarcode, self).create(vals)
        return res

    @api.one
    def generate_barcode(self):
        ean = generate_ean(str(self.id))
        new_barcode = mixBarcode(self, ean)
        if not self.barcode:
            self.barcode = new_barcode
        return True

    @api.multi
    def generate_barcodes(self):
        active_ids = self.env.context.get('active_ids', [])
        if active_ids:
            for i in range(len(active_ids)):
                obj = self.env['product.template'].browse([active_ids[i]])
                ean = generate_ean(str(obj.id))
                new_barcode = mixBarcode(obj, ean)
                # get record
                get_record = self.search([('id', '=', active_ids[i])])
                if not get_record.barcode:
                    get_record.write({'barcode': new_barcode})

        return True

def check_names(name):
    # if not len(name) >= 3:
    #     word_list = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    #     name += word_list[random.randint(0, (len(word_list) - 1))] + word_list[random.randint(0, (len(word_list) - 1))]
    # name = name[:3].lower()
    # return name
    word_list = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    categ_dict_list = {
        'clothing': 10,
        'cosmetic': 11,
        'fancy': 12,
        'foodbeverages': 13,
        'healthymedicine': 14,
        'household': 15,
        'phoneaccessories': 16,
        'stationary': 17,
        'bubbletea': 18,
    }

    categ_name = [i for i in list(name.lower()) if i in word_list]
    conf_categ_name = "".join([x for x in categ_name])
    if conf_categ_name in categ_dict_list:
        return categ_dict_list[conf_categ_name]
    return True

def mixBarcode(self, barcode):
    complex_0 = '0000'
    for record in self:
        record_name = record.categ_id.name
        categ_name = record.categ_id.parent_id.name
    # strip barcode to add character
    strip_barcode = "%s%s"%(barcode[:6], barcode[-1])

    get_categ_code = check_names(categ_name) if categ_name else check_names(record_name)
    if get_categ_code is not True:
        fbarcode = "{}{}{}".format(str(get_categ_code), complex_0, strip_barcode)
        return fbarcode
    if get_categ_code is True and record.barcode is False:
        raise UserError(_("Sorry, we cannot generate barcode for this category!"))
            
def ean_checksum(eancode):
    """returns the checksum of an ean string of length 13, returns -1 if the string has the wrong length"""
    if len(eancode) != 13:
        return -1
    oddsum = 0
    evensum = 0
    eanvalue = eancode
    reversevalue = eanvalue[::-1]
    finalean = reversevalue[1:]

    for i in range(len(finalean)):
        if i % 2 == 0:
            oddsum += int(finalean[i])
        else:
            evensum += int(finalean[i])
    total = (oddsum * 3) + evensum

    check = int(10 - math.ceil(total % 10.0)) % 10
    return check


def check_ean(eancode):
    """returns True if eancode is a valid ean13 string, or null"""
    if not eancode:
        return True
    if len(eancode) != 13:
        return False
    try:
        int(eancode)
    except:
        return False
    return ean_checksum(eancode) == int(eancode[-1])


def generate_ean(ean):
    """Creates and returns a valid ean13 from an invalid one"""
    if not ean:
        return "0000000000000"
    ean = re.sub("[A-Za-z]", "0", ean)
    ean = re.sub("[^0-9]", "", ean)
    ean = ean[:13]
    if len(ean) < 13:
        ean = ean + '0' * (13 - len(ean))
    return ean[:-1] + str(ean_checksum(ean))

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
