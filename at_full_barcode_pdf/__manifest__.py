# -*- coding: utf-8 -*-
{
    'name': "at_full_barcode_pdf",

    'author': "Kanaung Myanmar Software Solution",

    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'product'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'reports/report_views.xml',
        'reports/report_menu.xml',
        'assets.xml',
    ],
}
