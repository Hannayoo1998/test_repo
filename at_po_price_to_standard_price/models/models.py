from odoo import models, fields, api

class ProductUoM(models.Model):
    _inherit = 'product.uom'

    factor_inv = fields.Float(store = True)

class Picking(models.Model):
	_inherit = "stock.picking"

	@api.multi
	def change_product_cost_price(self, order_id):
		"""
			When user purchase an product or products with an order,
			set the cost_price of the product with purchased_price from order
			if that product is purchased for the first time
		"""
		OrderLines = self.env['purchase.order.line']
		Products = self.env['product.product']
		ProductTemplates = self.env['product.template']
		ProductUOMs = self.env['product.uom']
		PriceList = self.env['product.pricelist']
		PriceListItems = self.env['product.pricelist.item']
		PriceHistory = self.env['product.price.history']

		if not order_id:
			return
		order_lines = OrderLines.search(
										[('order_id', '=', order_id)], \
										order = 'product_id desc'
										)	# get purchase order items
		products_in_order = { str(product.product_id.id): product.price_unit for product in order_lines }		# get product and it's unit_price
		uom_of_each_product = { str(product.product_id.id): product.product_uom.id for product in order_lines }	# get product and it's uom
		product_ids = [i for i in products_in_order]		# collect product_ids
		product_templates = ProductTemplates.search([('id', 'in', product_ids)], order = "id desc") 
		ori_cost_prices = { str(i.id):i.standard_price for i in product_templates }		# original cost_price from product's template

		for i in ori_cost_prices:		#change cost price of each product if their	standard_price is zero
			for ii in products_in_order:
				if i == ii:
					ori_cost_prices[i] = products_in_order[i]	# change in dict
					uom_factor = ProductUOMs.search([('id', '=', uom_of_each_product[i])]).factor_inv \
								if ProductUOMs.search([('id', '=', uom_of_each_product[i])]).factor_inv > 0 else \
								ProductUOMs.search([('id', '=', uom_of_each_product[i])]).factor 	#check uom type and get cost_price of product
							
					#change in product form
					get_product_template = ProductTemplates.search([('id', '=', int(i))])
					get_product = Products.search([('id', '=', int(i))])
					if not get_product_template.standard_price:			
						exist = False
						get_product_template.standard_price = int(products_in_order[ii]) / uom_factor
						# get_product_template.list_price = int(products_in_order[ii]) / uom_factor
						used_pricelist = PriceList.search([('id', '=', get_product_template.default_pricelist.id)], limit = 1)		# get pricelist used in product form to know what pricelist product used
						get_pricelist_items = PriceListItems.search([('pricelist_id', '=', used_pricelist.id)], order = "id")
						for ii in get_pricelist_items:
							if ii.applied_on == "3_global":
								get_sale_discount = PriceHistory.calculate_final_price(ii, get_product)
								exist = True
							if ii.categ_id.id:
								get_categ_id_products  = Products.search([('product_tmpl_id.categ_id', '=', ii.categ_id.id)])
								list_categ_products = [i.id for i in get_categ_id_products]
								if get_product.id in list_categ_products:
									get_sale_discount = PriceHistory.calculate_final_price(ii, get_product)
									exist = True
							if ii.product_id.id:
								get_productss  = Products.search([('id', '=', ii.product_id.id)])
								list_products = [i.id for i in get_productss]
								if get_product.id in list_products:
									get_sale_discount = PriceHistory.calculate_final_price(ii, get_product)
									exist = True
							if ii.product_tmpl_id.id:
								get_t_productss  = Products.search([('product_tmpl_id', '=', ii.product_tmpl_id.id)])
								list_t_products = [i.id for i in get_t_productss]
								if get_product_template.id in list_t_products:
									get_sale_discount = PriceHistory.calculate_final_price(ii, get_product)
									exist = True
							
						if exist is not True:				# if product not in pricelist, set sale_discount to 0
							get_product_template.write({
								'sale_discount': 0,
								'list_price': get_product_template.standard_price
							})
		return True

	@api.multi
	def button_validate(self):
		PurchaseOrder = self.env['purchase.order']
		source_PO = PurchaseOrder.search([('name', '=', self.origin)]).id
		self.change_product_cost_price(source_PO)
		return super(Picking, self).button_validate()		# return origin method
		
